# History of wwrecking
Keep in mind I wrote this summary sometime in early 2020. The only changes I made were adding October 2021, removed a slur and the other swears in case Roblox reads this, and converted this to Markdown.
## 2007-2017: 
Literally nothing happened. The account was completely deserted by the original owner. I doubt he/she even still plays ROBLOX anymore.

## Late 2018 (bristallion/hitthenailonthehead):
I found a database containing tons of old ROBLOX accounts and their associated emails. Some of these emails either got deleted somehow, or just didn't exist. There was a trick where you could create an email with the same name as the emails which were used to create the old ROBLOX accounts. I tried several of these, and most didn't work. I kept trying until I came across one called "bristallion". To my surprise, this one actually worked. I then just decided to use the account to play mostly Prove it for whatever reason. I also played RUN and enjoyed the last days of non FE with this account.

## August-September 2019 (wwrecking):
I wanted to play Void Script Builder after not having played it since 2016. I was nervous to join on my main account since I was worried I would be banned within minutes and hated by everyone, so I looked through my alts to see which one was old enough to play, and one that I still have access to. wwrecking was the only option, because my only other accessible alt that was old enough was BonziBuddy32Alt, which is my main backup alt.

At first, I just sort of chilled. I didn't run any scripts.

I later created what would become my Notive edit. It was an absolute MESS of code, that would make even YandereDev's projects look good. I despise this script.
But still, some people didn't mind it. Some even LIKED it. I don't know why.

Notive then got banned from Void SB (keep in mind I never have and never will join the Void SB discord, so I don't know what's banned and what isn't), so it was time for the next script to get obsessed over, Anti Skid(TM)!

Yes, that's right, the legendary wwrecking's Anti Skid v4 Edit. I created it as an improved version of Anti Skid that detected far more scripts, including Infinite Yield SS, Grab Knife (all versions), Noob Switcher, Notive, Golden Pan, and that annoying remote script. It also contained more commands, such as >kick and >ban, >block and >allow for adjusting the level of script-blocking, and >banish.

Again, I just sort of chilled, now with my Anti Skid(TM) technology.

This is a little mini event, but I think it's important enough to go in here. Someone kept running toadroast, and Taphly blamed me for it every single time. I don't know if this blaming was satire or not, but regardless, I was offended.

Eventually, a user came along by the name of ErrorMode1. He warned me not to use Anti Skid as it was banned, but he conveyed the message so poorly that made me think he was trying to troll me. He used acronyms like "EOF", which makes no sense when trying to describe a script being banned. I joined a new server, and ran the script again. He said "stop it", and then crashed my client. At that point, I was shocked. He wasn't messing around.

I joined a new server once again, and went AFK on my Cloud9 workspace. When I came back, lo and behold, I got a 2 week ban from Void SB for running an "inappropriate script". How wonderful! The script name wasn't specified, so I assumed it was Anti Skid.

I never came back, even when my ban expired.

## October 2019 - 2020:
Dead. All I use wwrecking for is for checking on the account, and maybe reading the web chat. If I ever join Void SB again, it will probably be on my main.
I played Empty Baseplate one time on this account, but that's about it.

## October 2021
I made some updates to the wwrecking account. I logged in using the Tor Browser, because I was very nervous about doing this whole thing.
* I changed the description of my profile and wwrecking's Module to provide a link to this GitLab repository. I left the Old Robloxians group, because I didn't really join in 2007, and staying in there felt scummy. I do like that group, though.
    * I didn't leave Name Snipes Awakening, though. I felt that since the group was locked, I'll leave the account in there as history or something. I don't know.
* I removed the Cartoony Animation pack from my avatar.
* I updated the old "br1stall1on" group and repurposed it for a memorial group to discuss the wwrecking account.
    * I also moved it to my main account, so I can manage the group easier, and actually talk to other people without needing to log into the wwrecking account. I hope this doesn't get me terminated, but it probably will.
* I made the inventory public. There's no good reason to hide it if I'm opening up everything now.
* I didn't update the wwrecking's Module that's uploaded to Roblox, as that requires me to log into Studio, something I can't do over Tor (if I could, it would be very, *very* slow). I use the VPN I currently have with my main account, and I'm nervous about Roblox linking the two. I don't know. I'm just so scared.

I decided to change my mind about the memorial group thing. I don't really want to have a group dedicated to a cringe era of my life. I spent 25 Robux on a new role, and risked losing my account again for nothing. That's just great.

I feel so horrible now. I just risked getting my main account banned for something I didn't end up wanting.
