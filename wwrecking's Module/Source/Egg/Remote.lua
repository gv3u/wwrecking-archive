--set name
script.Parent.Name = "wwrecking's Module [PUBLIC RELEASE]"

--Prepare the skiddy skid scripts
local scriptFolder = {}

for i,v in pairs(script.Parent.SkidScripts:GetChildren()) do
	table.insert(scriptFolder,v:Clone())
end

script.Parent.SkidScripts:Destroy()

--""Special"" scripts
local globalScripts = {"asv4"}
local nameScripts = {"asv4", "f3x"}

--Remote Stuff
function Execute(plr, typedScript)
local typedStr = tostring(typedScript)
	for i = 1, #scriptFolder do
		if scriptFolder[i].Name:sub(1,typedStr:len()):lower() == typedStr:lower() then
			local typedScript = scriptFolder[i]:Clone()
			for i2, v2 in pairs(globalScripts) do
				if typedScript.Name == v2 then
					local ok = game.ServerScriptService:FindFirstChild(v2)
					if ok then ok:Destroy() end
					typedScript.Parent = game.ServerScriptService
				else
       				typedScript.Parent = plr:FindFirstChildOfClass("PlayerGui") or plr.Character;
				end
			end
			for i3, v3 in pairs(nameScripts) do
				if typedScript.Name == v3 then
					script.Parent.Owner:Clone().Parent = typedScript
				end
			end	
			typedScript.Disabled = false
		end
	end
end

script.ExecuteSkidScript.OnServerEvent:connect(Execute)