--This is IDDQD, an antideath based off the R6 converter.
--It doesn't protect against parenting your char to nil, but should work against everything else.
--Be sure to replace Player with your name.

local existing = game.ServerScriptService:FindFirstChild'IDDQDScript'
if existing then existing:Destroy() end
local IDDQDScript = script.IDDQDScript:Clone()
IDDQDScript.Parent = game.ServerScriptService
IDDQDScript.Disabled = false
