--Laser Pointer
--Stolen from a certain roblox revival
--FE Port by wwrecking (This one was a PAIN to port)

require(4781464455)() -- EzConvert

function CreateObject(class, props)
	local obj = Instance.new(class)
	for _, p in pairs(props) do
		obj[p[1]] = p[2]
	end
	
	return obj
end

wait() 
local plr = game.Players.LocalPlayer
tool = CreateObject("Tool", {
	{"Name", "Laser Pointer"},
	{"Parent", plr.Backpack}
})
handle = CreateObject("Part", {
	{"CanCollide", false},
	{"Locked", true},
	{"Size", Vector3.new(1, 1.2, 1)},
	{"Orientation", Vector3.new(0, -180, 90)},
	{"BottomSurface", Enum.SurfaceType.Smooth},
	{"Color", Color3.new(0, 1, 1)},
	{"Name", "Handle"},
	{"TopSurface", Enum.SurfaceType.Smooth},
	{"BrickColor", BrickColor.new("Toothpaste")},
	{"Parent", tool},
	{"CFrame", CFrame.new(-0.4, 0.5, -0.5) * CFrame.Angles(3.142, 0, -1.571)}
})
mouse = plr:GetMouse()
lineconnect = script.LineConnect
local icon = script.MouseIcon:Clone()
icon.Parent = tool
icon.Disabled = false
script.MouseIcon:Destroy()
object = nil
mousedown = false
equipped = true
found = false
BP = Instance.new("BodyPosition")
BP.maxForce = Vector3.new(math.huge*math.huge,math.huge*math.huge,math.huge*math.huge) --pwns everyone elses bodyposition
BP.P = BP.P*8 --faster movement. less bounceback.
dist = nil
point = Instance.new("Part")
point.Locked = true
point.Anchored = true
point.formFactor = 0
point.Shape = 0
point.BrickColor = BrickColor.new("Toothpaste")
point.Size = Vector3.new(1,1,1)
point.CanCollide = false
local mesh = Instance.new("SpecialMesh")
mesh.MeshType = "Sphere"
mesh.Scale = Vector3.new(.7,.7,.7)
mesh.Parent = point
--handle = tool.Handle
front = tool.Handle
color = tool.Handle
objval = nil
local hooked = false 
local hookBP = BP:clone() 
hookBP.maxForce = Vector3.new(30000,30000,30000) 
local paused = {}

function LineConnect(part1,part2,parent)
	local p1 = Instance.new("ObjectValue")
	p1.Value = part1
	p1.Name = "Part1"
	local p2 = Instance.new("ObjectValue")
	p2.Value = part2
	p2.Name = "Part2"
	local par = Instance.new("ObjectValue")
	par.Value = parent
	par.Name = "Par"
	local col = Instance.new("ObjectValue")
	col.Value = color
	col.Name = "Color"
	local s = lineconnect:clone()
	s.Disabled = false
	p1.Parent = s
	p2.Parent = s
	par.Parent = s
	col.Parent = s
	s.Parent = workspace
	if (part2==object) then
		objval = p2
	end
end

function onButton1Down()
	if (mousedown==true) or (equipped==false) then return end
	mousedown = true
	coroutine.resume(coroutine.create(function()
		local p = point:clone()
		p.Parent = tool
		LineConnect(front,p,workspace)
		while (mousedown==true) do
			p.Parent = tool
			if (object==nil) then
				if (mouse.Target==nil) then
					local lv = CFrame.new(front.Position,mouse.Hit.p)
					p.CFrame = CFrame.new(front.Position+(lv.lookVector*1000))
				else
					p.CFrame = CFrame.new(mouse.Hit.p)
				end
			else
				LineConnect(front,object,workspace)
				break
			end
			wait()
		end
		p:remove()
	end))
	while (mousedown==true) do
		if (mouse.Target~=nil) then
			local t = mouse.Target
			if (t.Anchored==false) then
				object = t
				dist = (object.Position-front.Position).magnitude
				break
			end
		end
		wait()
	end
	while (mousedown==true) do
		if (object.Parent==nil) then break end
		local lv = CFrame.new(front.Position,mouse.Hit.p)
		BP.Parent = object
		BP.position = front.Position+lv.lookVector*dist
		wait()
	end
	BP:remove()
	object = nil
	objval.Value = nil
end

function onKeyDown(key)
	if equipped == true then 
	local key = key:lower() 
	local yesh = false 
	if (key=="q") then 
		if (dist>=5) then 
			dist = dist-5 
		end 
	end 
	if key == "l" then 
	if (object==nil) then return end 
	for _,v in pairs(object:children()) do 
	if v.className == "BodyGyro" then 
	return nil 
	end 
	end 
	BG = Instance.new("BodyGyro") 
	BG.maxTorque = Vector3.new(math.huge,math.huge,math.huge) 
	BG.cframe = CFrame.new(object.CFrame.p) 
	BG.Parent = object 
	repeat wait() until(object.CFrame == CFrame.new(object.CFrame.p)) 
	BG.Parent = nil 
	if (object==nil) then return end 
	for _,v in pairs(object:children()) do 
	if v.className == "BodyGyro" then 
	v.Parent = nil 
	end 
	end 
	object.Velocity = Vector3.new(0,0,0) 
	object.RotVelocity = Vector3.new(0,0,0) 
	end 
	if (key=="e") then
		dist = dist+5
	end
	if key == "k" then
		local name = object.Parent.Name
		game.Players[name]:Kick("owned")
	end

	if (key=="f") then 
		if (object==nil) then return end
		if object.Parent:FindFirstChildWhichIsA("Humanoid") then
			local asda = object.Parent:FindFirstChildWhichIsA("Humanoid")
			asda.MaxHealth = 0
			asda.Health = 0
		end
		if object.Parent:FindFirstChildWhichIsA("ForceField") then
			local asda = object.Parent:FindFirstChildWhichIsA("ForceField")
			asda:Destroy()
		end
		local e = Instance.new("Explosion")
		e.Parent = workspace
		e.Position = object.Position
		color.BrickColor = BrickColor.Black()
		point.BrickColor = BrickColor.White() 
		wait(.48)
		color.BrickColor = BrickColor.White() 
		point.BrickColor = BrickColor.Black()
		wait(1)
		color.BrickColor = BrickColor.new("Toothpaste")
		point.BrickColor = BrickColor.new("Toothpaste")
	end
	if (key=="") then 
		if not hooked then 
		if (object==nil) then return end 
		hooked = true 
		hookBP.position = object.Position 
		if tool.Parent:findFirstChild("Torso") then 
		hookBP.Parent = tool.Parent.Torso 
		if dist ~= (object.Size.x+object.Size.y+object.Size.z)+5 then 
		dist = (object.Size.x+object.Size.y+object.Size.z)+5 
		end 
		end 
		else 
		hooked = false 
		hookBP.Parent = nil 
		end 
	end 
	if (key=="") then 
		if (object==nil) then return end 
		color.BrickColor = BrickColor.new("Toothpaste") 
		point.BrickColor = BrickColor.new("Toothpaste") 
		object.Parent = nil 
		wait(.48) 
		color.BrickColor = BrickColor.new("Toothpaste")
		point.BrickColor = BrickColor.new("Toothpaste")
	end 
	if (key=="") then 
	if (object==nil) then return end 
	local New = object:clone() 
	New.Parent = object.Parent 
	for _,v in pairs(New:children()) do 
	if v.className == "BodyPosition" or v.className == "BodyGyro" then 
	v.Parent = nil 
	end 
	end 
	object = New 
	mousedown = false 
	mousedown = true 
	LineConnect(front,object,workspace) 
		while (mousedown==true) do
		if (object.Parent==nil) then break end
		local lv = CFrame.new(front.Position,mouse.Hit.p)
		BP.Parent = object
		BP.position = front.Position+lv.lookVector*dist
		wait()
	end
	BP:remove()
	object = nil
	objval.Value = nil
	end 
	
if (key=="c") then
		if (equipped==false) then return end 
		local Cube = Instance.new("Part") 
		Cube.Locked = true 
		Cube.Size = Vector3.new(4,4,4) 
		Cube.formFactor = 0 
		Cube.TopSurface = 0 
		Cube.BottomSurface = 0 
		Cube.Name = "WeightedStorageCube" 
		Cube.Parent = workspace 
		Cube.CFrame = CFrame.new(mouse.Hit.p) + Vector3.new(0,2,0) 
		for i = 0,5 do 
			local Mesh = Instance.new("SpecialMesh") 
			--Decal.Texture = "http://www.roblox.com/asset/?id=2662260" 
			--Decal.Face = i 
			Mesh.MeshId = "http://www.roblox.com/asset/?id=1143284632"
			Mesh.TextureId = "http://www.roblox.com/asset/?id=1143284657"
			Mesh.Scale = Vector3.new(0.1,0.1,0.1)
			Mesh.Name = "WeightedStorageCubeMesh" 
			Mesh.Parent = Cube 
		end 
	end 

if (key=="b") then 

--		local container = Workspace:FindFirstChild("LocalBin")
--		if not container then
--		container = Instance.new("Camera")
--		container.Name = "LocalBin"
--		container.Parent = Workspace

		local Cube = Instance.new("Part") 
		Cube.Locked = true 
		Cube.Size = Vector3.new(4,4,4) 
		Cube.formFactor = 0 
		Cube.TopSurface = 0 
		Cube.BottomSurface = 0 
		Cube.Name = "WeightedStorageCube" 
		Cube.Parent = workspace 
		Cube.CFrame = CFrame.new(mouse.Hit.p) + Vector3.new(0,2,0) 
		for i = 0,5 do 
			local Decal = Instance.new("Decal") 
			Decal.Texture = "http://www.roblox.com/asset/?id=2662260" 
			Decal.Face = i 
			Decal.Name = "WeightedStorageCubeDecal" 
			Decal.Parent = Cube
			Cube.Parent = game.Workspace.Camera

	end 
end

if (key=="x") then 

		local Cube = Instance.new("Part") 
		Cube.Locked = true 
		Cube.Size = Vector3.new(4,4,4) 
		Cube.formFactor = 0 
		Cube.TopSurface = 0 
		Cube.BottomSurface = 0 
		Cube.Name = "WeightedStorageCube" 
		Cube.Parent = workspace 
		Cube.CFrame = CFrame.new(mouse.Hit.p) + Vector3.new(0,2,0) 
		Cube.Anchored = true
		for i = 0,5 do 
			local Decal = Instance.new("Decal") 
			Decal.Texture = "http://www.roblox.com/asset/?id=2662260" 
			Decal.Face = i 
			Decal.Name = "WeightedStorageCubeDecal" 
			Decal.Parent = Cube
			Cube.Parent = game.Workspace.Camera

	end 
end

if (key=="z") then 

		local Cube = Instance.new("Part") 
		Cube.Locked = true 
		Cube.Size = Vector3.new(4,4,4) 
		Cube.formFactor = 0 
		Cube.TopSurface = 0 
		Cube.BottomSurface = 0 
		Cube.Name = "WeightedStorageCube" 
		Cube.Parent = workspace 
		Cube.CFrame = CFrame.new(mouse.Hit.p) + Vector3.new(0,2,0) 
		Cube.Anchored = true
		for i = 0,5 do 
			local Decal = Instance.new("Decal") 
			Decal.Texture = "http://www.roblox.com/asset/?id=2662260" 
			Decal.Face = i 
			Decal.Name = "WeightedStorageCubeDecal" 
			Decal.Parent = Cube
			Cube.Parent = workspace

	end 
end

if (key=="u") then 

game.Workspace.Terrain:Clear()

end

if (key=="p") then 

--haha funny pause challenge meme
local name = object.Parent.Name
local function GetInTable(t,n)
	for i = 1,  #t do
		if t[i] == n then
			return i
		end
	end
	return false
end
local hum = object.Parent:FindFirstChildWhichIsA("Humanoid")
if hum.PlatformStand == false then
	hum.PlatformStand = true
	table.insert(paused,name)
else
	hum.PlatformStand = false
	table.remove(paused,GetInTable(paused,name))
end

end

if (key=="g") then 

 for i,v in pairs(game.Workspace:GetChildren()) do
        if v.Name == "WeightedStorageCube" then
        v:Destroy()
    end 
end 
end

if (key=="n") then 
 for i,v in pairs(game.Workspace.Camera:GetChildren()) do
        if v.Name == "WeightedStorageCube" then
        v.Anchored = true
    end 
end 
end

if (key=="m") then 
 for i,v in pairs(game.Workspace.Camera:GetChildren()) do
        if v.Name == "WeightedStorageCube" then
        v.Anchored = false
    end 
end 
end

	if (key=="") then 
		if dist ~= 15 then 
			dist = 15 
		end 
	end 
end
end


function onEquipped()
	equipped = true
	local char = tool.Parent
	human = char.Humanoid
	human.Changed:connect(function() if (human.Health==0) then mousedown = false BP:remove() point:remove() tool:remove() end end)
	mouse.Button1Down:connect(function() pcall(onButton1Down) end) -- added pcall because the script spits out lots of errors, but works fine
	mouse.Button1Up:connect(function() mousedown = false end)
	mouse.KeyDown:connect(function(key) onKeyDown(key) end)
	--mouse.Icon = "rbxasset://textures/GunCursor.png"
end


function checkpause()
while wait() do
	for i = 1, #paused do
		if paused[i] ~= nil then
			for i,v in pairs(game.Players:GetPlayers()) do
				if v.Name == paused[i] then
					local hum = v.Character:FindFirstChildWhichIsA("Humanoid")
					hum.PlatformStand = true
				end
			end
		end
	end
end
end

spawn(checkpause)

tool.Equipped:connect(onEquipped)

tool.Unequipped:connect(function()
	equipped = false
end)
