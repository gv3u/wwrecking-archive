--[[
	wwrecking's Anti Skid v4 Edit
	
	This was it. The script that gave me a 2 week ban. (a fortNITE HAHAHAHAHAHA HAH AAHhHAHAHAAGGGAGAGGGAGAGgaagafsd)
	Have fun. Go wild. Poke fun at me.
	wwrecking's Module and its corresponding (original) scripts are released under the MIT License.
	
	ORIGINAL SCRIPT: https://pastebin.com/d39gi5i1
	Script made by Rufus14
	Edit made by wwrecking
	It's Spaghetti But It Works™
	oh and before you say it, this anti skid edit has existed longer than you think
--]]

--[[
	List of scripts that CANNOT be toggled:
	666
	toadroast
	you are an idiot
	Grab Knife v1/v3/v4
	Infinite Yield SS
	Cool Modules GUI
	Ultimate Trolling GUI
	Big Smoke (The reason this is blocked is because it's laggy as hell.)
	(PityGUI/PityHub won't be added.)
	(Nebula's stuff also won't be added.)
	Notive
--]]

--[[
	List of things you CAN toggle:
	Loud audios (loudaudio)
	That remote control tool thing (remote)
	Switcher V2 (switcher)
	nahid Chara or Jevil (nahidchara)
	Noob Switcher (noobswitcher)
	Golden Pan (gpan)
	Xester (xester)
	Trap Gun/Rifle (trapgun)
	Dual Doom (dualdoom)
	Everyone's favorite, Big Ban. (bigban)
	Skyboxes (skyboxes)
	Everything (all)
]]
	
--[[
	Changelog:
	
	5/??/2019
	
	Added IY SS, Cool Modules GUI, Ultimate Trolling GUI, and Big Smoke to the non toggleable blacklist
	Also added new epic gui yes that basically taunts people that run non toggleable blacklisted scripts
	
	6/4/2019
	
	finally fixed the annoying bug where it would only detect non toggleable scripts once
	
	6/7/2019
	
	Made nahid chara/jevil and noob switcher separate detections, also made xester toggleable and just basic general improvements
	
	7/11/2019
	
	removed owner-only toggle for non toggleable scripts since it is pointless, like me
	
	7/15/2019
	
	Added the ability to toggle skybox blocking, and experimental antideath
	
	7/19/2019
	
	Notive is now added to the non toggleable list.
	Also fixed a bug where glitchers etc can destroy the humanoid and break Degresslessness Mode. (haha jokes on you this is related to something completely unrelated that I have no idea how to fix)
	
	7/24/2019
	Added in latest features from AntiSkid update.
	
	7/27/2019
	
	Hopefully fixed the problem where scan2 borks when Degreelessness Mode is enabled.
	
	8/3/2019
	Added in latest features from AntiSkid update. (Didn't even notice this one lol)
	also finally added a commands list (IDDQD is unlisted for obvious reeasons.)
	I didn't add toggle notify, because let's be honest, who would want to turn off my (and rufus's) beautiful messages?
	
	9/5/2019
	Got a 2 week ban for using this script that I have ALWAYS USED (even in front of mods) mind you.
	
	4|18|2020
	Cleaned up for release, also obfuscated.

	10|1|2020
	Upload to git.gv3u.dev, unobfuscated.
--]]

script.Name = "wwrecking's Anti Skid v4 Edit."
script.Parent = game.ServerScriptService
repeat wait() until script.Parent == game.ServerScriptService


owner = game.Players:WaitForChild(script.Owner.Value) -- If you're not using this with wwrecking's Module, replace script.Owner.Value with your name.
wait(.5)

--variables
cannotify = true
blockloudaudio = false
unloading = false
blockall = false
blocknotive = false
blockbban = false
blockgayremotes = false
blockswitcher = false
blockdualdoomguy = false
blocksmellyandlaggyglitchers = false
blockopandlaggyscript = false
blocknahidcharaandjevil = false
destroydisgustingweebs = false
blockxester = false
blockdeadmeme = false
blockgpan = false
blockskyboxes = false
canragdollkill = true
reason = "You have been kicked by the glorious Egg."
verybadskids = {}
banned = {}
ew = {}

--the meat
function message(msg)
	if cannotify then
		cannotify = false
		local messagesound = Instance.new("Sound", workspace)
		messagesound.SoundId = "rbxassetid://651986414"
		messagesound.Volume = 5.999
		messagesound.Name = "msg"
		messagesound:Play()
		game.Debris:AddItem(messagesound, messagesound.TimeLength)
		if owner.Character then
			if owner.Character:findFirstChild("HumanoidRootPart") then
				local pos1 = math.random(-5,5)
				local pos2 = math.random(-5,5)
				local model = Instance.new("Model", owner.Character)
				model.Name = [[{wwrecking's asv4 Edit}
				]]..msg
				local human0 = Instance.new("Humanoid", model)
				human0.Name = "m"
				human0.MaxHealth = 0
				human0.Health = 0
				local part = Instance.new("Part", model)
				part.Size = Vector3.new(0,0,0)
				part.BrickColor = BrickColor.Random()
				part.Material = "ForceField"
				part:BreakJoints()
				part.CanCollide = false
				part.CFrame = owner.Character.HumanoidRootPart.CFrame
				part.Name = "Head"
				local bodyposition = Instance.new("BodyPosition", part)
				bodyposition.MaxForce = Vector3.new(math.huge,math.huge,math.huge)
				bodyposition.Position = owner.Character.HumanoidRootPart.Position
				local function spinandpos()
					while wait() do
						if owner.Character:findFirstChild("HumanoidRootPart") then
							bodyposition.Position = owner.Character:findFirstChild("HumanoidRootPart").Position + Vector3.new(pos1,0,pos2)
						end
						part.CFrame = part.CFrame * CFrame.fromEulerAnglesXYZ(0.02,0.02,0.02)
					end
				end
				local function resize()
					while wait() and part.Size.x < 1.5 do
						part.Size = part.Size + Vector3.new(0.05,0.05,0.05)
					end
					wait(4)
					for i = 1,30 do
						part.Size = part.Size + Vector3.new(0.05,0.05,0.05)
						part.Transparency = part.Transparency + 0.015
						wait()
					end
					model:destroy()
				end
				local function let()
					wait(0.5)
					cannotify = true
				end
				spawn(let)
				spawn(spinandpos)
				spawn(resize)
			end
		end
	end
end

function ragdollkill(character)
	local victimshumanoid = character:findFirstChildOfClass("Humanoid")
	if character:findFirstChild("wooosh") then
		character:findFirstChild("wooosh"):destroy()
	end
	if not character:findFirstChild("UpperTorso") then
		character.Archivable = true
		for i,v in pairs(character:GetChildren()) do
			if v.ClassName == "Sound" then
				v:remove()
			end
			for q,w in pairs(v:GetChildren()) do
				if w.ClassName == "Sound" and w.Name ~= "chinese" and w.Name ~= "booom" then
					w:remove()
				end
			end
		end
		local ragdoll = character:Clone()
		ragdoll:findFirstChildOfClass("Humanoid").Health = 0
		if ragdoll:findFirstChild("Health") then
			if ragdoll:findFirstChild("Health").ClassName == "Script" then
				ragdoll:findFirstChild("Health").Disabled = true
			end
		end
		for i,v in pairs(character:GetChildren()) do
			if v.ClassName == "Part" or v.ClassName == "ForceField" or v.ClassName == "Accessory" or v.ClassName == "Hat" then
				v:destroy()
			end
		end
		for i,v in pairs(character:GetChildren()) do
			if v.ClassName == "Accessory" then
				local attachment1 = v.Handle:findFirstChildOfClass("Attachment")
				if attachment1 then
					for q,w in pairs(character:GetChildren()) do
						if w.ClassName == "Part" then
							local attachment2 = w:findFirstChild(attachment1.Name)
							if attachment2 then
								local hinge = Instance.new("HingeConstraint", v.Handle)
								hinge.Attachment0 = attachment1
								hinge.Attachment1 = attachment2
								hinge.LimitsEnabled = true
								hinge.LowerAngle = 0
								hinge.UpperAngle = 0
							end
						end
					end
				end
			end
		end
		ragdoll.Parent = workspace
		if ragdoll:findFirstChild("Right Arm") then
			local glue = Instance.new("Glue", ragdoll.Torso)
			glue.Part0 = ragdoll.Torso
			glue.Part1 = ragdoll:findFirstChild("Right Arm")
			glue.C0 = CFrame.new(1.5, 0.5, 0, 0, 0, 1, 0, 1, 0, -1, 0, 0)
			glue.C1 = CFrame.new(0, 0.5, 0, 0, 0, 1, 0, 1, 0, -1, 0, 0)
			local limbcollider = Instance.new("Part", ragdoll:findFirstChild("Right Arm"))
			limbcollider.Size = Vector3.new(1.4,1,1)
			limbcollider.Shape = "Cylinder"
			limbcollider.Transparency = 1
			limbcollider.Name = "LimbCollider"
			local limbcolliderweld = Instance.new("Weld", limbcollider)
			limbcolliderweld.Part0 = ragdoll:findFirstChild("Right Arm")
			limbcolliderweld.Part1 = limbcollider
			limbcolliderweld.C0 = CFrame.fromEulerAnglesXYZ(0,0,math.pi/2) * CFrame.new(-0.3,0,0)
		end
		if ragdoll:findFirstChild("Left Arm") then
			local glue = Instance.new("Glue", ragdoll.Torso)
			glue.Part0 = ragdoll.Torso
			glue.Part1 = ragdoll:findFirstChild("Left Arm")
			glue.C0 = CFrame.new(-1.5, 0.5, 0, 0, 0, -1, 0, 1, 0, 1, 0, 0)
			glue.C1 = CFrame.new(0, 0.5, 0, 0, 0, -1, 0, 1, 0, 1, 0, 0)
			local limbcollider = Instance.new("Part", ragdoll:findFirstChild("Left Arm"))
			limbcollider.Size = Vector3.new(1.4,1,1)
			limbcollider.Shape = "Cylinder"
			limbcollider.Name = "LimbCollider"
			limbcollider.Transparency = 1
			local limbcolliderweld = Instance.new("Weld", limbcollider)
			limbcolliderweld.Part0 = ragdoll:findFirstChild("Left Arm")
			limbcolliderweld.Part1 = limbcollider
			limbcolliderweld.C0 = CFrame.fromEulerAnglesXYZ(0,0,math.pi/2) * CFrame.new(-0.3,0,0)
		end
		if ragdoll:findFirstChild("Left Leg") then
			local glue = Instance.new("Glue", ragdoll.Torso)
			glue.Part0 = ragdoll.Torso
			glue.Part1 = ragdoll:findFirstChild("Left Leg")
			glue.C0 = CFrame.new(-0.5, -1, 0, -0, -0, -1, 0, 1, 0, 1, 0, 0)
			glue.C1 = CFrame.new(-0, 1, 0, -0, -0, -1, 0, 1, 0, 1, 0, 0)
			local limbcollider = Instance.new("Part", ragdoll:findFirstChild("Left Leg"))
			limbcollider.Size = Vector3.new(1.4,1,1)
			limbcollider.Shape = "Cylinder"
			limbcollider.Name = "LimbCollider"
			limbcollider.Transparency = 1
			local limbcolliderweld = Instance.new("Weld", limbcollider)
			limbcolliderweld.Part0 = ragdoll:findFirstChild("Left Leg")
			limbcolliderweld.Part1 = limbcollider
			limbcolliderweld.C0 = CFrame.fromEulerAnglesXYZ(0,0,math.pi/2) * CFrame.new(-0.3,0,0)
		end
		if ragdoll:findFirstChild("Right Leg") then
			local glue = Instance.new("Glue", ragdoll.Torso)
			glue.Part0 = ragdoll.Torso
			glue.Part1 = ragdoll:findFirstChild("Right Leg")
			glue.C0 = CFrame.new(0.5, -1, 0, 0, 0, 1, 0, 1, 0, -1, -0, -0)
			glue.C1 = CFrame.new(0, 1, 0, 0, 0, 1, 0, 1, 0, -1, -0, -0)
			local limbcollider = Instance.new("Part", ragdoll:findFirstChild("Right Leg"))
			limbcollider.Size = Vector3.new(1.4,1,1)
			limbcollider.Shape = "Cylinder"
			limbcollider.Name = "LimbCollider"
			limbcollider.Transparency = 1
			local limbcolliderweld = Instance.new("Weld", limbcollider)
			limbcolliderweld.Part0 = ragdoll:findFirstChild("Right Leg")
			limbcolliderweld.Part1 = limbcollider
			limbcolliderweld.C0 = CFrame.fromEulerAnglesXYZ(0,0,math.pi/2) * CFrame.new(-0.3,0,0)
		end
		if ragdoll:findFirstChild("Head") and ragdoll.Torso:findFirstChild("NeckAttachment") then
			local HeadAttachment = Instance.new("Attachment", ragdoll["Head"])
			HeadAttachment.Position = Vector3.new(0, -0.5, 0)
			local connection = Instance.new('HingeConstraint', ragdoll["Head"])
			connection.LimitsEnabled = true
			connection.Attachment0 = ragdoll.Torso.NeckAttachment
			connection.Attachment1 = HeadAttachment
			connection.UpperAngle = 60
			connection.LowerAngle = -60
		elseif ragdoll:findFirstChild("Head") and not ragdoll.Torso:findFirstChild("NeckAttachment") then
			local hedweld = Instance.new("Weld", ragdoll.Torso)
			hedweld.Part0 = ragdoll.Torso
			hedweld.Part1 = ragdoll.Head
			hedweld.C0 = CFrame.new(0,1.5,0)
		end
		game.Debris:AddItem(ragdoll, 5)
		local function aaaalol()
			wait(0.2)
			local function searchforvelocity(wot)
				for i,v in pairs(wot:GetChildren()) do
					searchforvelocity(v)
					if v.ClassName == "BodyPosition" or v.ClassName == "BodyVelocity" then
						v:destroy()
					end
				end
			end
			searchforvelocity(ragdoll)
			wait(0.5)
			if ragdoll:findFirstChildOfClass("Humanoid") then
				ragdoll:findFirstChildOfClass("Humanoid").PlatformStand = true
			end
			if ragdoll:findFirstChild("HumanoidRootPart") then
				ragdoll:findFirstChild("HumanoidRootPart"):destroy()
			end
		end
		spawn(aaaalol)
	elseif character:findFirstChild("UpperTorso") then
		character.Archivable = true
		for i,v in pairs(character:GetChildren()) do
			if v.ClassName == "Sound" then
				v:remove()
			end
			for q,w in pairs(v:GetChildren()) do
				if w.ClassName == "Sound" and w.Name ~= "chinese" and w.Name ~= "booom" then
					w:remove()
				end
			end
		end
		local ragdoll = character:Clone()
		ragdoll:findFirstChildOfClass("Humanoid").Health = 0
		if ragdoll:findFirstChild("Health") then
			if ragdoll:findFirstChild("Health").ClassName == "Script" then
				ragdoll:findFirstChild("Health").Disabled = true
			end
		end
		for i,v in pairs(character:GetChildren()) do
			if v.ClassName == "Part" or v.ClassName == "ForceField" or v.ClassName == "Accessory" or v.ClassName == "Hat" or v.ClassName == "MeshPart" then
				v:destroy()
			end
		end
		for i,v in pairs(character:GetChildren()) do
			if v.ClassName == "Accessory" then
				local attachment1 = v.Handle:findFirstChildOfClass("Attachment")
				if attachment1 then
					for q,w in pairs(character:GetChildren()) do
						if w.ClassName == "Part" or w.ClassName == "MeshPart" then
							local attachment2 = w:findFirstChild(attachment1.Name)
							if attachment2 then
								local hinge = Instance.new("HingeConstraint", v.Handle)
								hinge.Attachment0 = attachment1
								hinge.Attachment1 = attachment2
								hinge.LimitsEnabled = true
								hinge.LowerAngle = 0
								hinge.UpperAngle = 0
							end
						end
					end
				end
			end
		end
		ragdoll.Parent = workspace
		local Humanoid = ragdoll:findFirstChildOfClass("Humanoid")
		Humanoid.PlatformStand = true
		local function makeballconnections(limb, attachementone, attachmenttwo, twistlower, twistupper)
			local connection = Instance.new('BallSocketConstraint', limb)
			connection.LimitsEnabled = true
			connection.Attachment0 = attachementone
			connection.Attachment1 = attachmenttwo
			connection.TwistLimitsEnabled = true
			connection.TwistLowerAngle = twistlower
			connection.TwistUpperAngle = twistupper
			local limbcollider = Instance.new("Part", limb)
			limbcollider.Size = Vector3.new(0.1,1,1)
			limbcollider.Shape = "Cylinder"
			limbcollider.Transparency = 1
			limbcollider:BreakJoints()
			local limbcolliderweld = Instance.new("Weld", limbcollider)
			limbcolliderweld.Part0 = limb
			limbcolliderweld.Part1 = limbcollider
			limbcolliderweld.C0 = CFrame.fromEulerAnglesXYZ(0,0,math.pi/2)
		end
		local function makehingeconnections(limb, attachementone, attachmenttwo, lower, upper)
			local connection = Instance.new('HingeConstraint', limb)
			connection.LimitsEnabled = true
			connection.Attachment0 = attachementone
			connection.Attachment1 = attachmenttwo
			connection.LimitsEnabled = true
			connection.LowerAngle = lower
			connection.UpperAngle = upper
			local limbcollider = Instance.new("Part", limb)
			limbcollider.Size = Vector3.new(0.1,1,1)
			limbcollider.Shape = "Cylinder"
			limbcollider.Transparency = 1
			limbcollider:BreakJoints()
			local limbcolliderweld = Instance.new("Weld", limbcollider)
			limbcolliderweld.Part0 = limb
			limbcolliderweld.Part1 = limbcollider
			limbcolliderweld.C0 = CFrame.fromEulerAnglesXYZ(0,0,math.pi/2)
		end
		if Humanoid.Parent:findFirstChild("Head") then
			HeadAttachment = Instance.new("Attachment", Humanoid.Parent.Head)
			HeadAttachment.Position = Vector3.new(0, -0.5, 0)
		end
		if ragdoll.UpperTorso:findFirstChild("NeckAttachment") then
			makehingeconnections(Humanoid.Parent.Head, HeadAttachment, ragdoll.UpperTorso.NeckAttachment, -50, 50)
		end
		makehingeconnections(Humanoid.Parent.LowerTorso, Humanoid.Parent.LowerTorso.WaistRigAttachment, Humanoid.Parent.UpperTorso.WaistRigAttachment, -50, 50)
		makeballconnections(Humanoid.Parent.LeftUpperArm, Humanoid.Parent.LeftUpperArm.LeftShoulderRigAttachment, Humanoid.Parent.UpperTorso.LeftShoulderRigAttachment, -200, 200, 180)
		makehingeconnections(Humanoid.Parent.LeftLowerArm, Humanoid.Parent.LeftLowerArm.LeftElbowRigAttachment, Humanoid.Parent.LeftUpperArm.LeftElbowRigAttachment, 0, -60)
		makehingeconnections(Humanoid.Parent.LeftHand, Humanoid.Parent.LeftHand.LeftWristRigAttachment, Humanoid.Parent.LeftLowerArm.LeftWristRigAttachment, -20, 20)
		--
		makeballconnections(Humanoid.Parent.RightUpperArm, Humanoid.Parent.RightUpperArm.RightShoulderRigAttachment, Humanoid.Parent.UpperTorso.RightShoulderRigAttachment, -200, 200, 180)
		makehingeconnections(Humanoid.Parent.RightLowerArm, Humanoid.Parent.RightLowerArm.RightElbowRigAttachment, Humanoid.Parent.RightUpperArm.RightElbowRigAttachment, 0, -60)
		makehingeconnections(Humanoid.Parent.RightHand, Humanoid.Parent.RightHand.RightWristRigAttachment, Humanoid.Parent.RightLowerArm.RightWristRigAttachment, -20, 20)
		--
		makeballconnections(Humanoid.Parent.RightUpperLeg, Humanoid.Parent.RightUpperLeg.RightHipRigAttachment, Humanoid.Parent.LowerTorso.RightHipRigAttachment, -80, 80, 80)
		makehingeconnections(Humanoid.Parent.RightLowerLeg, Humanoid.Parent.RightLowerLeg.RightKneeRigAttachment, Humanoid.Parent.RightUpperLeg.RightKneeRigAttachment, 0, 60)
		makehingeconnections(Humanoid.Parent.RightFoot, Humanoid.Parent.RightFoot.RightAnkleRigAttachment, Humanoid.Parent.RightLowerLeg.RightAnkleRigAttachment, -20, 20)
		--
		makeballconnections(Humanoid.Parent.LeftUpperLeg, Humanoid.Parent.LeftUpperLeg.LeftHipRigAttachment, Humanoid.Parent.LowerTorso.LeftHipRigAttachment, -80, 80, 80)
		makehingeconnections(Humanoid.Parent.LeftLowerLeg, Humanoid.Parent.LeftLowerLeg.LeftKneeRigAttachment, Humanoid.Parent.LeftUpperLeg.LeftKneeRigAttachment, 0, 60)
		makehingeconnections(Humanoid.Parent.LeftFoot, Humanoid.Parent.LeftFoot.LeftAnkleRigAttachment, Humanoid.Parent.LeftLowerLeg.LeftAnkleRigAttachment, -20, 20)
		for i,v in pairs(Humanoid.Parent:GetChildren()) do
			if v.ClassName == "Accessory" then
				local attachment1 = v.Handle:findFirstChildOfClass("Attachment")
				if attachment1 then
					for q,w in pairs(Humanoid.Parent:GetChildren()) do
						if w.ClassName == "Part" then
							local attachment2 = w:findFirstChild(attachment1.Name)
							if attachment2 then
								local hinge = Instance.new("HingeConstraint", v.Handle)
								hinge.Attachment0 = attachment1
								hinge.Attachment1 = attachment2
								hinge.LimitsEnabled = true
								hinge.LowerAngle = 0
								hinge.UpperAngle = 0
							end
						end
					end
				end
			end
		end
		for i,v in pairs(ragdoll:GetChildren()) do
			for q,w in pairs(v:GetChildren()) do
				if w.ClassName == "Motor6D"--[[ and w.Name ~= "Neck"--]] then
					w:destroy()
				end
			end
		end
		if ragdoll:findFirstChild("HumanoidRootPart") then
			ragdoll.HumanoidRootPart.Anchored = true
			ragdoll.HumanoidRootPart.CanCollide = false
		end
		if ragdoll:findFirstChildOfClass("Humanoid") then
			ragdoll:findFirstChildOfClass("Humanoid").PlatformStand = true
		end
		local function waitfordatmoment()
			wait(0.2)
			local function searchforvelocity(wot)
				for i,v in pairs(wot:GetChildren()) do
					searchforvelocity(v)
					if v.ClassName == "BodyPosition" or v.ClassName == "BodyVelocity" then
						v:destroy()
					end
				end
			end
			searchforvelocity(ragdoll)
		end
		spawn(waitfordatmoment)
		game.Debris:AddItem(ragdoll, 5)
	end
end


function punishplayer(character)
	if not character:findFirstChild("pwned") then
		Instance.new("BoolValue", character).Name = "pwned"
		local function DieSkid()
		local punishrandom = math.random(1,7)
		for i,v in pairs(character:GetChildren()) do
			if v.ClassName == "Sound" then
				v:destroy()
			end
			for q,w in pairs(v:GetChildren()) do
				if w.ClassName == "Sound" and w.Name ~= "JevilTheme" then
					w:destroy()
				end
			end
		end
		if punishrandom == 1 then
			for i = 1,20 do
				for q,w in pairs(character:GetChildren()) do
					if w.ClassName == "Part" then
						w.Transparency = w.Transparency + 0.05
					end
					if w.ClassName == "Accessory" then
						if w:findFirstChild("Handle") then
							w.Handle.Transparency = w.Handle.Transparency + 0.05
						end
					end
					if w.Name == "Head" then
						if w:findFirstChildOfClass("Decal") then
							w:findFirstChildOfClass("Decal").Transparency = w:findFirstChildOfClass("Decal").Transparency + 0.05
						end
					end
				end
				wait()
			end
			for q,w in pairs(character:GetChildren()) do
				if w.ClassName == "Part" or w.ClassName == "Accessory" then
					w:destroy()
				end
			end
		elseif punishrandom == 2 then
			character:findFirstChildOfClass("Humanoid").WalkSpeed = 0
			character:findFirstChildOfClass("Humanoid").JumpPower = 0
			local function addweld()
				if character:findFirstChild("Torso") then
					local heead = Instance.new("Weld", character.Torso)
					heead.Part0 = character.Torso
					heead.Part1 = character.Head
					heead.C0 = CFrame.new(0,1.5,0)
					heead.Name = "HeadWeld"
					wait(0.7)
					for i = 0,1 , 0.08 do
						heead.C0 = heead.C0:lerp(CFrame.new(0,1.5,0.4) * CFrame.fromEulerAnglesXYZ(1.2,0,0),i)
						wait()
					end
					wait(0.7)
					while wait() do
						heead.C0 = heead.C0 * CFrame.fromEulerAnglesXYZ(0.5,0.5,0.5)
					end
				elseif character:findFirstChild("UpperTorso") then
					local heead = Instance.new("Weld", character.UpperTorso)
					heead.Part0 = character.UpperTorso
					heead.Part1 = character.Head
					heead.C0 = CFrame.new(0,1.5,0)
					heead.Name = "HeadWeld"
					wait(0.7)
					for i = 0,1 , 0.08 do
						heead.C0 = heead.C0:lerp(CFrame.new(0,1.5,0.4) * CFrame.fromEulerAnglesXYZ(1.2,0,0),i)
						wait()
					end
					wait(0.7)
					while wait() do
						heead.C0 = heead.C0 * CFrame.fromEulerAnglesXYZ(0.5,0.5,0.5)
					end
				end
			end
			spawn(addweld)
			local banhamma = Instance.new("Part", character)
			banhamma.Size = Vector3.new(3, 10, 2)
			banhamma:BreakJoints()
			banhamma.CFrame = character.Head.CFrame * CFrame.new(0,20,0) * CFrame.fromEulerAnglesXYZ(math.pi/2,0,1)
			banhamma.Name = "ban"
			banhamma.Transparency = 1
			banhamma.Anchored = true
			local mesh = Instance.new("SpecialMesh", banhamma)
			mesh.MeshId = "http://www.roblox.com/asset/?id=10604848"
			mesh.TextureId = "http://www.roblox.com/asset/?id=10605252"
			local special = Instance.new("Sound", banhamma)
			special.SoundId = "rbxassetid://427090157"
			special.Volume = 2
			special:Play()
			for i = 1,20 do
				banhamma.Transparency = banhamma.Transparency - 0.05
				wait()
			end
			local hammertime = Instance.new("Sound", banhamma)
			hammertime.SoundId = "rbxassetid://176192087"
			hammertime.Volume = 3
			hammertime:Play()
			wait(1)
			if character:findFirstChild("Head") then
				local scream = Instance.new("Sound", character.Head)
				scream.SoundId = "rbxassetid://176238381"
				scream.Volume = 2
				scream:Play()
				banhamma.Anchored = false
				local angularvel = Instance.new("BodyAngularVelocity", banhamma)
				angularvel.MaxTorque = Vector3.new(math.huge,math.huge,math.huge)
				angularvel.AngularVelocity = banhamma.CFrame.rightVector * 20
				local touchedban = false
				local function touched(part)
					if part.Name == "Head" and part.Parent == character then
						touchedban = true
						local aaaaaaaa = Instance.new("Sound", banhamma)
						aaaaaaaa.SoundId = "rbxassetid://147722910"
						aaaaaaaa.Volume = 2
						aaaaaaaa:Play()
						angularvel:Destroy()
						banhamma.CanCollide = true
						for i,v in pairs(character:GetChildren()) do
							if v.ClassName == "Part" and v.Name ~= "ban" or v.ClassName == "Accessory" then
								v:destroy()
							end
						end
					end
				end
				banhamma.Touched:connect(touched)
				wait(2)
				if not touchedban and character:findFirstChild("Head") then
					banhamma.CFrame = character:findFirstChild("Head").CFrame
				end
			end
		elseif punishrandom == 3 then
			character:BreakJoints()
			character:findFirstChildOfClass("Humanoid").Health = 0
			local explo = Instance.new("Explosion", character)
			explo.Position = character.Head.Position
			explo.BlastPressure = 0
			local boom = Instance.new("Sound", character.Head)
			boom.SoundId = "rbxassetid://262562442"
			boom.Volume = 3
			boom:Play()
			for i,v in pairs(character:GetChildren()) do
				if v.ClassName == "Part" or v.ClassName == "MeshPart" then
					local velocity = Instance.new("BodyVelocity", v)
					velocity.MaxForce = Vector3.new(math.huge,math.huge,math.huge)
					velocity.Velocity = Vector3.new(math.random(-60,60),math.random(-60,60),math.random(-60,60))
					game.Debris:AddItem(velocity, 0.5)
				end
			end
			wait(2)
			if canragdollkill then
				character:BreakJoints()
				ragdollkill(character)
			else
				if character:FindFirstChild'Head' then
					character:FindFirstChild'Head':Destroy()
				end
			end
		elseif punishrandom == 4 then
			character.Head:destroy()
			if character:findFirstChild("HumanoidRootPart") then
				local boom = Instance.new("Sound", character.HumanoidRootPart)
				boom.SoundId = "rbxassetid://775395533"
				boom.Volume = 1.5
				boom:Play()
			end
			if canragdollkill then
				character:BreakJoints()
				ragdollkill(character)
			end
		--Added in from latest AntiSkid update
		elseif punishrandom == 5 then
				local hahahahahah = Instance.new("Sound", workspace)
				hahahahahah.SoundId = "rbxassetid://807874496"
				hahahahahah.Volume = 1.5
				hahahahahah:Play()
				game.Debris:AddItem(hahahahahah, hahahahahah.TimeLength)
				if character:findFirstChild("HumanoidRootPart") then
					local humanoidrootpart = Instance.new("Weld", character.HumanoidRootPart)
					humanoidrootpart.Part0 = character.HumanoidRootPart
					if character:findFirstChild("Torso") then
						humanoidrootpart.Part1 = character.Torso
					elseif character:findFirstChild("UpperTorso") then
						humanoidrootpart.Part1 = character.UpperTorso
					end
					humanoidrootpart.Name = "HumanoidRootPartWeld"
					local function rrreeeeee()
						while humanoidrootpart do
							humanoidrootpart.C0 = humanoidrootpart.C0 * CFrame.Angles(math.random(-2,2),math.random(-2,2),math.random(-2,2))
							wait()
						end
					end
					spawn(rrreeeeee)
				end
				local function thot()
					for i = 1,20 do
						for q,w in pairs(character:GetChildren()) do
							if w.ClassName == "Part" then
								w.Transparency = w.Transparency + 0.05
							end
							if w.ClassName == "Accessory" then
								if w:findFirstChild("Handle") then
									w.Handle.Transparency = w.Handle.Transparency + 0.05
								end
							end
							if w.Name == "Head" then
								if w:findFirstChildOfClass("Decal") then
									w:findFirstChildOfClass("Decal").Transparency = w:findFirstChildOfClass("Decal").Transparency + 0.05
								end
							end
						end
						wait()
					end
					for q,w in pairs(character:GetChildren()) do
						if w.ClassName == "Part" or w.ClassName == "Accessory" then
							w:destroy()
						end
					end
				end
				spawn(thot)
			elseif punishrandom == 6 then
				for q,w in pairs(character:GetChildren()) do
					if w.ClassName == "Part" or w.ClassName == "MeshPart" then
						w.Anchored = true
					end
					if w.Name == "Health" and w.ClassName == "Script" then
						w.Disabled = true
					end
				end
				local aaaaaaaaa = Instance.new("Sound", character.Head)
				aaaaaaaaa.SoundId = "rbxassetid://3181950107"
				aaaaaaaaa.Volume = 3
				aaaaaaaaa.Name = 'Loud noises funny, upvotes to the left'
				aaaaaaaaa:Play()
				aaaaaaaaa.TimePosition = 2.9
				local hydraulicpress = Instance.new("Part", character)
				hydraulicpress.Size = Vector3.new(12,6,12)
				hydraulicpress.Anchored = true
				hydraulicpress.Material = "CorrodedMetal"
				hydraulicpress.CFrame = character.Head.CFrame * CFrame.new(0,20,0)
				local hydraulicpress2 = Instance.new("Part", character)
				hydraulicpress2.Size = Vector3.new(3,400,3)
				hydraulicpress2.Anchored = true
				hydraulicpress2.Material = "CorrodedMetal"
				hydraulicpress2.CFrame = hydraulicpress.CFrame * CFrame.new(0,hydraulicpress2.Size.y/2,0)
				for i = 1,9 do
					hydraulicpress2.CFrame = hydraulicpress2.CFrame * CFrame.new(0,-2.4,0)
					hydraulicpress.CFrame = hydraulicpress.CFrame * CFrame.new(0,-2.4,0)
					game:GetService("RunService").Stepped:wait()
				end
				if character:findFirstChildOfClass("Humanoid") then
					character:findFirstChildOfClass("Humanoid").Health = 0.01
				end
				wait(3)
				character:BreakJoints()
			elseif punishrandom == 7 then
				local congrats_pleb = Instance.new("Sound", character.Head)
				congrats_pleb.SoundId = "rbxassetid://360099761"
				congrats_pleb.Volume = 5.999
				congrats_pleb.Name = "congratzpleb"
				local notorbitalflutesound = Instance.new("Sound", character.Head)
				notorbitalflutesound.SoundId = "rbxassetid://225627419"
				notorbitalflutesound.Volume = 5.999
				notorbitalflutesound.Name = "congratzpleb1"
				notorbitalflutesound:Play()
				congrats_pleb:Play()
				game.Debris:AddItem(notorbitalflutesound, 0.3)
				game.Debris:AddItem(congrats_pleb, 3)
				-- Farewell Infortality.
				-- Version: 2.82
				-- Instances:
				local ScreenGui = Instance.new("ScreenGui")
				game.Debris:AddItem(ScreenGui,2.5)
				local h = Instance.new("TextLabel")
				--Properties:
				ScreenGui.Name = "ąęśżźółń"
				ScreenGui.Parent = game:GetService("Players"):findFirstChild(character.Name):findFirstChildOfClass("PlayerGui")
				ScreenGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
				
				h.Name = "h ę"
				h.Parent = ScreenGui
				h.BackgroundColor3 = Color3.new(1, 1, 1)
				h.BackgroundTransparency = 1
				h.Size = UDim2.new(1, 0, 1, 0)
				h.Font = Enum.Font.SourceSansLight
				h.TextColor3 = Color3.new(1, 1, 1)
				h.TextScaled = true
				h.TextSize = 14
				h.TextWrapped = true
				h.Text = "Congratulations,"
				wait(1)
				h.Text = "you played yourself."
				wait(0.8)
				if character:findFirstChild("Head") then
					--Converted with ttyyuu12345's model to script plugin v4
					local function sandbox(var,func)
						local env = getfenv(func)
						local newenv = setmetatable({},{
							__index = function(self,k)
								if k=="script" then
									return var
								else
									return env[k]
								end
							end,
						})
						setfenv(func,newenv)
						return func
					end
					local cors = {}
					local mas = Instance.new("Model",game:GetService("Lighting"))
					local fluuute = Instance.new("Part")
					local SpecialMesh1 = Instance.new("SpecialMesh")
					fluuute.Name = "wooosh"
					fluuute.Parent = mas
					fluuute.Anchored = true
					fluuute.CFrame = CFrame.new(-9.54999828, 10.7131758, -2.17677665, 1, 0, 0, 0, 0.707106709, 0.707106709, 0, -0.707106709, 0.707106709)
					fluuute.Orientation = Vector3.new(-45, 0, 0)
					fluuute.Position = Vector3.new(-9.54999828, 10.7131758, -2.17677665)
					fluuute.Rotation = Vector3.new(-45, 0, 0)
					fluuute.Size = Vector3.new(0.099999927, 2.4000001, 0.0999999568)
					fluuute.BottomSurface = Enum.SurfaceType.Smooth
					fluuute.TopSurface = Enum.SurfaceType.Smooth
					fluuute.FormFactor = Enum.FormFactor.Custom
					fluuute.formFactor = Enum.FormFactor.Custom
					fluuute.CanCollide = false
					SpecialMesh1.Parent = fluuute
					SpecialMesh1.MeshId = "rbxassetid://223104924"
					SpecialMesh1.Scale = Vector3.new(0.25, 0.25, 0.25)
					SpecialMesh1.TextureId = "rbxassetid://223104978"
					SpecialMesh1.MeshType = Enum.MeshType.FileMesh
					for i,v in pairs(mas:GetChildren()) do
						v.Parent = character
						pcall(function() v:MakeJoints() end)
					end
					mas:Destroy()
					for i,v in pairs(cors) do
						spawn(function()
							pcall(v)
						end)
					end
					fluuute.CFrame = character.Head.CFrame * CFrame.new(math.random(-600,600),600,math.random(-600,600))
					Instance.new("Fire", fluuute)
					local orbitalwoosh = Instance.new("Sound", fluuute)
					orbitalwoosh.SoundId = "rbxassetid://97179624"
					orbitalwoosh.Volume = 5.999
					orbitalwoosh:Play()
					orbitalwoosh.Looped = true
					orbitalwoosh.PlaybackSpeed = 0.9
					local canchase = true
					local function flyto()
						while game:GetService("RunService").Stepped:wait() and character do
							if character:findFirstChild("Head") then
								if (character.Head.Position - fluuute.Position).magnitude > 1.5 and canchase then
									fluuute.CFrame = CFrame.new(fluuute.Position, character:findFirstChild("Head").Position) * CFrame.Angles(-math.pi/2,0,0)
									fluuute.CFrame = fluuute.CFrame * CFrame.new(0,1.5,0)
									if fluuute:findFirstChild("ray") then
										fluuute:findFirstChild("ray"):destroy()
									end
									local raye = Ray.new(fluuute.CFrame.p, fluuute.CFrame.upVector * 1000)
									local hit, position = workspace:FindPartOnRayWithIgnoreList(raye, {fluuute})
									local partte = Instance.new("Part", fluuute)
									partte.Anchored = true
									partte.CanCollide = false
									partte.Material = "Neon"
									partte.BrickColor = BrickColor.new("Really red")
									partte.Name = "ray"
									partte.Size = Vector3.new(0,0,(fluuute.Position - position).magnitude)
									partte.CFrame = CFrame.new(fluuute.Position, position) * CFrame.new(0,0,-(fluuute.Position - position).magnitude/2)
								else
									if character then
										if canchase and character:findFirstChild("Head") then
											canchase = false
											local Sound2 = Instance.new("Sound", workspace)
											Sound2.Name = "Explosion"
											Sound2.SoundId = "rbxassetid://55224766"
											Sound2:Play()
											orbitalwoosh:Stop()
											fluuute.Transparency = 1
											if fluuute:FindFirstChildOfClass("Fire") then
												fluuute:FindFirstChildOfClass("Fire"):destroy()
											end
											game.Debris:AddItem(Sound2, Sound2.TimeLength)
											game.Debris:AddItem(fluuute, Sound2.TimeLength)
											local boom = Instance.new("Explosion", workspace)
											boom.BlastRadius = 0
											boom.Position = character.Head.Position
											character:BreakJoints()
											for i,v in pairs(character:GetChildren()) do
												if v.ClassName == "Part" or v.ClassName == "MeshPart" then
													v.BrickColor = BrickColor.new("Black")
													Instance.new("Fire", v)
												end
											end
											if canragdollkill then
												ragdollkill(character)
											end
										end
									end
								end
							end
						end
					end
					local function higherpitch()
						orbitalwoosh.PlaybackSpeed = orbitalwoosh.PlaybackSpeed + 0.2
					end
					orbitalwoosh.DidLoop:connect(higherpitch)
					spawn(flyto)
					end
		end
		end
		spawn(DieSkid)
	end
end

function iveranoutoffunnynamesforfunctionsok(GUI,msgtext)
	local Troll = script.bad:Clone()
	Troll.Parent = GUI
	Troll.Enabled = true
	Troll.ok.Disabled = false
	Troll.yes.msg.Text = msgtext
end


function scan2()
	while wait() do
		
		if unloading == true then break end
	for i,v in pairs(game.Players:GetPlayers()) do
		if v.Character then
			if not v.Character:findFirstChild("pwned") then
	--begin non toggleable scripts
					--grab knife v1
					if v.Backpack:findFirstChild("Grab") then
						if v.Backpack.Grab:IsA("HopperBin") then
						table.insert(verybadskids,v.Name)
						v:LoadCharacter()
						message(v.Name.." has used Grab Knife v1.")
						iveranoutoffunnynamesforfunctionsok(v.PlayerGui,[[
Hi!
We've detected that you are using:
Grab Knife v1
Is this true?
						]])
						table.insert(ew,v.Name)
						end
					end
					--grab knife v3
					for i2,v2 in pairs(workspace:GetChildren()) do
						if v2.Name == "bitch ass knife" and v2:IsA("Model") then
							if v.Character.Head:findFirstChild("Psycho") then
								table.insert(verybadskids,v.Name)
								v:LoadCharacter()
								message(v.Name.." has used Grab Knife v3.")
								iveranoutoffunnynamesforfunctionsok(v.PlayerGui,[[
Hi!
We've detected that you are using:
Grab Knife v3
Is this true?
								]])
								table.insert(ew,v.Name)
							end
						end
					end
					--grab knife v4
					if v.Character:findFirstChild("handle") then
						if v.Character.handle.BrickColor == BrickColor.new("Really black") then
							if v.PlayerGui:findFirstChild("MainGUI") then
								table.insert(verybadskids,v.Name)
								v:LoadCharacter()
								message(v.Name.." has used Grab Knife v4.")
								iveranoutoffunnynamesforfunctionsok(v.PlayerGui,[[
Hi!
We've detected that you are using:
Grab Knife v4
Is this true?
								]])
								table.insert(ew,v.Name)
								v.PlayerGui["MainGUI"]:Destroy()
							end
						end
					end
					--IY SS
					if v.PlayerGui:FindFirstChild("IY_GUI") and not v.PlayerGui:FindFirstChild("CoolModulesGUI") then
						table.insert(verybadskids,v.Name)
						v:LoadCharacter()
						message(v.Name.." has used Infinite Yield SS.")
						iveranoutoffunnynamesforfunctionsok(v.PlayerGui,[[
Hi!
We've detected that you are using:
Infinite Yield SS
Is this true?
						]])
						table.insert(ew,v.Name)
						v.PlayerGui["IY_GUI"]:Destroy()
					end
					--UTG
					if v.PlayerGui:FindFirstChild("AccessUI") or v.PlayerGui:FindFirstChild("UTG") then
						table.insert(verybadskids,v.Name)
						v:LoadCharacter()
						message(v.Name.." has used Ultimate Trolling GUI.")
						iveranoutoffunnynamesforfunctionsok(v.PlayerGui,[[
Hi!
We've detected that you are using:
Ultimate Trolling GUI
Is this true?
						]])
						table.insert(ew,v.Name)
						if v.PlayerGui:FindFirstChild("AccessUI") then
							v.PlayerGui["AccessUI"]:Destroy()
						else
							v.PlayerGui["UTG"]:Destroy()
						end
					end
					--CMG
					if v.PlayerGui:FindFirstChild("CoolModulesGUI") then
						table.insert(verybadskids,v.Name)
						v:LoadCharacter()
						message(v.Name.." has used Cool Modules GUI.")
						iveranoutoffunnynamesforfunctionsok(v.PlayerGui,[[
Hi!
We've detected that you are using:
Cool Modules GUI (or something similar)
Is this true?
						]])
						table.insert(ew,v.Name)
						if v.PlayerGui:FindFirstChild("IY_GUI") then
							v.PlayerGui["IY_GUI"]:Destroy()
							v.PlayerGui["CoolModulesGUI"]:Destroy()
						else
						v.PlayerGui["CoolModulesGUI"]:Destroy()
						end
					end
					--big smoke
					if v.Character:findFirstChild'Head' then
					if v.Character.Head:findFirstChild("Health Display") then
							if v.Character.Head["Health Display"].Frame:findFirstChild("TextLabel") then
								v.Character.Head["Health Display"].Frame.TextLabel.Text = "stop using this laggy script"
								punishplayer(v.Character)
								message(v.Name.." has used Big Smoke.")
							end
					end
					end
					--Notive (This detects ALL versions of Notive, even "Noteve")
					if v.Character:findFirstChild("feaqurmam") then
						v.Character:FindFirstChild("_status").TextLabel.Text = "stop using this dead meme script"
						punishplayer(v.Character)
						message(v.Name.." has used Notive.")
					end
					
	--end non toggleable scripts
		end
	end
	end
	end
end

spawn(scan2)


function scan()
	while wait() do
		if unloading == true then break end
		for i3,w in pairs(workspace:GetChildren()) do
			if blockloudaudio == true then
			if w.ClassName == "Sound" and w.Name ~= "Sound" and not w:findFirstChild("muted") then
				if w.Volume >= 6 then
					Instance.new("BoolValue", w).Name = "muted"
					local function lolriplmao()
						while game:GetService("RunService").Heartbeat:wait() do
							w.Volume = 1
						end
					end
					spawn(lolriplmao)
					message("Detected loud audio coming from workspace")
				end
			end
		end
		end
		for i4,w2 in pairs(workspace:GetDescendants()) do
		if w2.ClassName == "BillboardGui" and w2.Name == "stuf" then
				message("666 script detected, cleaning workspace")
				wait(.5)
				--fix lighting
				spawn(function()
				game.Lighting.TimeOfDay = 17
				game.Lighting.Brightness = 1
				game.Lighting.ShadowColor = Color3.new("178, 178, 183")
				game.Lighting.Ambient = Color3.new("0, 0, 0")
				game.Lighting.FogEnd = 100000
				game.Lighting.FogColor = Color3.new("192, 192, 192")
				end)
				--clear workspace
				wait(.3)
			    for _,v in pairs(workspace:GetChildren()) do
					if v:IsA("Script") or v:IsA("Part") and v.Name ~= "Base" then
						v:Destroy()
					end
				end
		end
		if w2.ClassName == "Sound" and w2.Name == "RAINING MEN" or w2.ClassName == "Sound" and w2.SoundId == "rbxassetid://141509625" then
				message("Toadroast detected, cleaning workspace")
				wait(.5)
				w2:Stop()
				game.StarterGui:ClearAllChildren()
				game.Lighting:ClearAllChildren()
				for _,v in pairs(workspace:GetChildren()) do
					if v:IsA("Script") or v:IsA("Part") and v.Name ~= "Base" then
						v:Destroy()
					end
				end
				w2:Destroy()
			end
		end
		for idk,skids in pairs(game.Lighting:GetChildren()) do
			if skids.ClassName == "Sky" and skids.Name == "Gitrekt1!1!1" then
				message("You are an idiot script detected, fixing skybox and removing sound")
				wait(.5)
				for why,endme in pairs(workspace:GetChildren()) do
					if endme:IsA("Sound") or endme:IsA("Message") or endme:IsA("Script") then
						endme:Destroy()
					end
				end
				skids:Destroy()
			end
			if blockskyboxes == true or blockall == true then
				if skids.ClassName == "Sky" then
					message'Clearing skybox'
					skids:Destroy()
				end
			end
		end
		for i,v in pairs(game:GetService("Players"):GetChildren()) do		
			
			if v.Character then
				if blockloudaudio == true then
				for q,w in pairs(v.Character:GetChildren()) do
					if w.ClassName == "Sound" and not w:findFirstChild("muted") then
						if w.Volume >= 6 then
							Instance.new("BoolValue", w).Name = "muted"
							local function lolriplmao()
								while game:GetService("RunService").Heartbeat:wait() do
									w.Volume = 1
								end
							end
							spawn(lolriplmao)
							message("Detected loud audio coming from ".. v.Name)
						end
					end
					for e,r in pairs(w:GetChildren()) do
						if r.ClassName == "Sound" and not r:findFirstChild("muted") then
							if r.Volume >= 6 then
								Instance.new("BoolValue", r).Name = "muted"
								local function lolriplmao()
									while game:GetService("RunService").Heartbeat:wait() do
										r.Volume = 1
									end
								end
								spawn(lolriplmao)
								message("Detected loud audio coming from ".. v.Name)
							end
						end
					end
				end
				end
				
					
				if not v.Character:findFirstChild("pwned") then
					
					--ultra instict 2019
					if blockall == true then
					if v.Character:findFirstChild("Hair") then
						if v.Character.Hair:findFirstChild("Mesh") then
							if v.Character.Hair.Mesh.MeshId == "rbxassetid://455100513" then
								local function uncol()
									while game:GetService("RunService").Heartbeat:wait() do
										v.Character.Hair.CanCollide = false
									end
								end
								spawn(uncol)
								for q,w in pairs(v.Character:GetChildren()) do
									if w.Name == "NewPart" then
										w.CanCollide = false
									end
								end
								punishplayer(v.Character)
								message(v.Name.." has used Ultra Instict.")
							end
						end
					end
					end
					--nahid Chara/jevil
					if blocknahidcharaandjevil == true or blockall == true then
					if v.Character:findFirstChild("Blob") then
						local testaa = v.Character:findFirstChild("Name")
						if testaa then
							if testaa:findFirstChild("TextLabel") then
								if testaa.TextLabel.Text == "welcome to my nahid hell" or testaa.TextLabel.Text == "nahid Chara" or testaa.TextLabel.Text == "???" or testaa.TextLabel.Text == "J҉ E҉ V҉ I҉ L҉" then
								testaa.TextLabel.Text = "Sans Gaming"
								punishplayer(v.Character)
								message(v.Name.." has used nahid Chara/Jevil.")
								end
							end
						end
					end
					end
					--noob switcher
					if blockopandlaggyscript == true or blockall == true then
					if v.Character:findFirstChild("Blob") then
						local testaa = v.Character:findFirstChild("Name")
						if testaa then
							if testaa:findFirstChild("TextLabel") then
								if testaa.TextLabel.Text == "Script By Henriquegame2015 (Rare Script)(Leaked)" or testaa.TextLabel.Text == "Load....." or testaa.TextLabel.Text == "Noob Switcher" then
								testaa.TextLabel.Text = "this script is annoying as hell stop it"
								punishplayer(v.Character)
								message(v.Name.." has used Noob Switcher.")
								end
							end
						end
					end
					end
					--star glitcher
					if blocksmellyandlaggyglitchers == true or blockall == true then
					if v.Character:findFirstChild("Head") then
						if v.Character.Head:findFirstChild("ModeName") then
							if v.Character.Head:findFirstChild("ModeName"):findFirstChildOfClass("TextLabel") then
								v.Character.Head:findFirstChild("ModeName"):findFirstChildOfClass("TextLabel").Text = "you're smelly and lagging the server"
								punishplayer(v.Character)
								message(v.Name.." has used Star Glitcher. (or something similar)")
							end
						end
					end
					if v.Character:findFirstChildOfClass("Sound") then
						if v.Character:findFirstChildOfClass("Sound").SoundId == "rbxassetid://614032233" or v.Character:findFirstChildOfClass("Sound").SoundId == "rbxassetid://415898123" or v.Character:findFirstChildOfClass("Sound").Name == "wrecked" then
							punishplayer(v.Character)
							message(v.Name.." has used Star Glitcher. (or something similar")
						end
					end
					end
					--big ban theory
					if blockbban == true or blockall == true then
					if v.Character:findFirstChild("Hammer") then
						v.Character:findFirstChild("Hammer"):destroy()
						local theguy = game.Players:GetPlayerFromCharacter(v.Character)
						--destroy scripts so people can't use ban/
						for cuphead, andhispalmugman in pairs(theguy.PlayerGui:GetDescendants()) do if andhispalmugman:IsA("Script") or andhispalmugman:IsA("LocalScript") then andhispalmugman:Destroy() end end
						for icantthink, ofanythingfunny in pairs(theguy.Backpack:GetDescendants()) do if ofanythingfunny:IsA("Script") or ofanythingfunny:IsA("LocalScript") then ofanythingfunny:Destroy() end end
						punishplayer(v.Character)
						message(v.Name.." used Big Ban Theory.")
					end
					end
					--dual doom
					if blockdualdoomguy == true or blockall == true then
					if v.Character:findFirstChild("GunModel") then
						v.Character.GunModel:destroy()
						if v.Character:findFirstChild("GunModel") then
							v.Character.GunModel:destroy()
						end
						punishplayer(v.Character)
						message(v.Name.." used Dual Doom.")
					end
					end
					--jevil (im so sorry mrfunnylaughs)
					if blockall == true then
					if v.Character:findFirstChild("Torso") then
						if v.Character.Torso:findFirstChild("JevilTheme") then
							v.Character.Torso:findFirstChild("JevilTheme").SoundId = "rbxassetid://1953695670"
							v.Character.Torso:findFirstChild("JevilTheme").TimePosition = 5
							v.Character.Torso:findFirstChild("JevilTheme"):Play()
							punishplayer(v.Character)
							message(v.Name.." has used Jevil.")
						end
					end
					end
					--dead gun
					if blockall == true then
					if v.Character:findFirstChild("Torso") then
						if v.Character.Torso:findFirstChild("Sound") then
							if v.Character.Torso:findFirstChild("Sound").SoundId == "rbxassetid://318812395" then
								v.Character.Torso:findFirstChild("Sound").SoundId = "rbxassetid://775395533"
								punishplayer(v.Character)
								message(v.Name.." has used Dead Gun.")
							end
						end
					end
					end
					--guest glitcher
					if blockall == true or blocksmellyandlaggyglitchers == true then
					if v.Character:findFirstChild("wreckeda") then
						punishplayer(v.Character)
						message(v.Name.." has used Guest Glitcher/Galaxy Glitcher.")
					end
					end
					--xester
					if blockxester == true or blockall == true then
					if v.Character:findFirstChild("Torso") then
						if v.Character.Torso:findFirstChild("doomtheme") then
							punishplayer(v.Character)
							message(v.Name.." has used Xester.")
						end
					end
					end
					--disgusting weeb gun
					if destroydisgustingweebs == true or blockall == true then
						if v.Character:findFirstChild("LuvGun") then
							punishplayer(v.Character)
							message(v.Name.." has used the Trap Gun.")
						end
					end
					--switcher
					if blockswitcher == true or blockall == true then
						if v.Character:findFirstChild("_status") then
							if v.Character["_status"]:findFirstChild("TextLabel") then
								if v.Character["_status"].TextLabel.Text == "Switcher v.2" then
									v.Character["_status"].TextLabel.Text = "h"
									punishplayer(v.Chatacter)
									message(v.Name.." has used Switcher.")
								end
							end
						end
					end
					--remote
					if blockgayremotes == true or blockall == true then
					if v.Character:findFirstChild("Remote") and v.Character.Remote:IsA'Tool' then
						v.Character.Remote:Destroy()
						punishplayer(v.Character)
						message(v.Name.." has used the Remote.")
					end
					end
					--Golden Pan
					if blockgpan == true or blockall == true then
						if v.Character:FindFirstChild'bluE' then
							v.Character.bluE:Destroy()
							punishplayer(v.Character)
							message(v.Name..' has used the Golden Pan.')
						end
					end
					--Added in from latest AntiSkid update
					if blockall == true then
						
						--fat banisher
						if v.Character:findFirstChild("Adds") then
							v.Character:findFirstChild("Adds"):destroy()
							punishplayer(v.Character)
							v:LoadCharacter()
							message(v.Name.." fat skid has probably used a banisher.")
						end
						--gay ass eggdog
						if v.Character:findFirstChild("EggDog") then
							punishplayer(v.Character)
							message(v.Name.." has used Egg-Dog script.")
						end
						--flamingo meme moment
						if v.Character:findFirstChild("OofHead") then
							punishplayer(v.Character)
							message(v.Name.." has used a script from the worm family.")
							wait(1)
							message(v.Name.." is also an Albert fan. Subscribe to 1ik.")
						end
						--white shiny flying unkillable dominus thing
						if v.Character:findFirstChild("Control.mp3") then
							punishplayer(v.Character)
							v:LoadCharacter()
							message(v.Name.." has used that white shiny flying unkillable dominus thing.")
						end
						
					end
					--chaotic glicher
					if blockall == true or blocksmellyandlaggyglitchers == true then
					if v.Character:findFirstChild("_status") then
						if v.Character["_status"]:findFirstChild("TextLabel") then
							--if string.sub(v.Character["_status"]:findFirstChild("TextLabel").Text,1,5) == "Lunar" then
							if v.Character["_status"].TextLabel.Text == "Lunar" then
								v.Character["_status"].TextLabel.Text = "rip chaotic lol"
								punishplayer(v.Character)
								message(v.Name.." has used Chaotic Glicher.")
							end
						end
					end
					end
				end
			end
		end
	end
end

spawn(scan)





function GetInTable(t, n) --stolen
	for i = 1, #t do
		if t[i] == n then
			return i
		end
	end
end


function commands(chat)
if string.sub(chat, 1,5) == ">cmds" or string.sub(chat, 1,5) == ">help" or string.sub(chat, 1,14) == ">howdoiusethis" or string.sub(chat, 1,13) == ">aisdjfskadjf" then
	message("Commands: >kill plr, >ban/unban plr, >banish/unbanish plr,")
	wait(1.5)
	message(">block/allow script, >sandbox/unsandbox plr,")
    wait(1.5)
    message(">ragdoll on/off, >setreason msg,")
	wait(1.5)
	message("and all the normal AntiSkid cmds.")
	wait(1.5)
	message("Want a list of scripts? Read the code comments.")
end
if string.sub(chat, 1,8) == ">ragdoll" then
		if string.sub(chat, 10,11) == "on" then
			canragdollkill = true
			message("Ragdoll enabled.")
		end
		if string.sub(chat, 10,12) == "off" then
			message("Ragdoll disabled.")
			canragdollkill = false
		end
end
	if string.sub(chat, 1,5) == ">kill" then
		local ply = game:GetService("Players"):findFirstChild(string.sub(chat, 7))
		if ply then
			if ply.Name == "wwrecking" then
				ply:Kick("NO SMELLY EGGS ALLOWED.")
			else
				if ply.Character then
					punishplayer(ply.Character)
					message("Punished "..ply.Name..".")
				else
					message("Player didn't spawn yet.")
				end
			end
		else
			message("Cannot find the player named '"..string.sub(chat, 7).."'.")
		end
end
if string.sub(chat, 1,10) == ">setreason" then
	reason = string.sub(chat, 12)
end
if string.sub(chat, 1,5) == ">kick" then
	local ply = game:GetService("Players"):findFirstChild(string.sub(chat, 7))
	if ply then
		ply:Kick(reason)
		message(ply.Name.." has been kicked.")
	else
		message("Cannot find the player named '"..string.sub(chat, 7).."'.")
	end
end
if string.sub(chat, 1,4) == ">ban" and string.sub(chat, 5,7) ~= "ish" then
	local ply = game:GetService("Players"):findFirstChild(string.sub(chat, 6))
	if ply then
		table.insert(banned,ply.Name)
		message(ply.Name.." has been banned.")
		ply:Kick(reason)
	else
	message("Cannot find the player named '"..string.sub(chat, 6).."'.")
	end
end
if string.sub(chat, 1,6) == ">unban" and string.sub(chat, 7,9) ~= "ish" then
	if string.sub(chat, 8,11) == "all" then
		banned = {}
		message("Unbanned all")
	else
	local ply = string.sub(chat, 8)
	if ply then
		table.remove(banned,GetInTable(banned,ply.Name))
		message(ply.Name.." has been unbanned.")
	else
	message("Cannot find the player named '"..string.sub(chat, 8).."'.")
	end
	end
end

if string.sub(chat, 1,7) == ">banish" then
	local ply = game:GetService("Players"):findFirstChild(string.sub(chat, 9))
	if ply then
		punishplayer(ply.Character)
		table.insert(verybadskids,ply.Name)
		message(ply.Name.." has been banished.")
	else
	message("Cannot find the player named '"..string.sub(chat, 9).."'.")
	end
end
if string.sub(chat, 1,9) == ">unbanish" then
	if string.sub(chat, 11,14) == "all" then
		verybadskids = {}
		message("Unbanished all")
	else
	local ply = string.sub(chat, 11)
	if ply then
		table.remove(verybadskids,GetInTable(verybadskids,ply.Name))
		message(ply.Name.." has been unbanished.")
	else
	message("Cannot find the player named '"..string.sub(chat, 11).."'.")
	end
	end
end
	if string.sub(chat, 1,10) == ">unsandbox" then
		local ply = game:GetService("Players"):findFirstChild(string.sub(chat, 12))
		if ply then
			local lool = Instance.new("BoolValue", ply)
			lool.Name = "stopsandboxing"
			game.Debris:AddItem(lool, 0.5)
			message("Unsandboxed "..ply.Name..".")
		else
			message("Cannot find the player named '"..string.sub(chat, 12).."'.")
		end
	end
	if string.sub(chat, 1,8) == ">sandbox" then
		if string.sub(chat, 10,12) == "all" then
			for i,v in pairs(game:GetService("Players"):GetChildren()) do
				if v.Character then
					local ply = v
					modell = Instance.new("Model", workspace:FindFirstChildOfClass("Terrain"))
					modell.Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
					local scamhumanoid = Instance.new("Humanoid", modell)
					modell2 = Instance.new("Model", modell)
					modell2.Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
					ply.Character.Parent = modell2
					ply.Character:findFirstChildOfClass("Humanoid").Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
					local function checkifnil()
						if ply.Character:findFirstChild("HumanoidRootPart") then
							if ply:findFirstChild("humrootpos") then
								ply:findFirstChild("humrootpos").Value = ply.Character.HumanoidRootPart.CFrame
							else
								local posnow = Instance.new("CFrameValue", ply)
								posnow.Value = ply.Character.HumanoidRootPart.CFrame
								posnow.Name = "humrootpos"
							end
						end
						while wait() and not ply:findFirstChild("stopsandboxing") do
							if ply.Character then
								if ply.Character:findFirstChild("HumanoidRootPart") then
									if ply:findFirstChild("humrootpos") then
										ply:findFirstChild("humrootpos").Value = ply.Character.HumanoidRootPart.CFrame
									else
										local posnow = Instance.new("CFrameValue", ply)
										posnow.Value = ply.Character.HumanoidRootPart.CFrame
										posnow.Name = "humrootpos"
									end
								end
							end
							if ply.Character.Parent == nil or modell.Parent == nil or modell2.Parent == nil then
								ply:LoadCharacter()
								while not ply.Character do
									game:GetService("RunService").Heartbeat:wait()
								end
								ply.Character.HumanoidRootPart.CFrame = ply:findFirstChild("humrootpos").Value
								wait()
								modell = Instance.new("Model", workspace:FindFirstChildOfClass("Terrain"))
								modell.Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
								local scamhumanoid = Instance.new("Humanoid", modell)
								modell2 = Instance.new("Model", modell)
								modell2.Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
								ply.Character.Parent = modell2
								ply.Character:findFirstChildOfClass("Humanoid").Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
							end
						end
						print("stopped")
						ply:LoadCharacter()
						while not ply.Character do
							game:GetService("RunService").Heartbeat:wait()
						end
						ply.Character.HumanoidRootPart.CFrame = ply:findFirstChild("humrootpos").Value
						ply:findFirstChild("humrootpos"):destroy()
					end
					spawn(checkifnil)
					message("Separated every player from workspace.")
				end
			end
		else
			local ply = game:GetService("Players"):findFirstChild(string.sub(chat, 10))
			if ply then
				if ply.Character then
					modell = Instance.new("Model", workspace:FindFirstChildOfClass("Terrain"))
					modell.Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
					local scamhumanoid = Instance.new("Humanoid", modell)
					modell2 = Instance.new("Model", modell)
					modell2.Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
					ply.Character.Parent = modell2
					ply.Character:findFirstChildOfClass("Humanoid").Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
					local function checkifnil()
						if ply.Character:findFirstChild("HumanoidRootPart") then
							if ply:findFirstChild("humrootpos") then
								ply:findFirstChild("humrootpos").Value = ply.Character.HumanoidRootPart.CFrame
							else
								local posnow = Instance.new("CFrameValue", ply)
								posnow.Value = ply.Character.HumanoidRootPart.CFrame
								posnow.Name = "humrootpos"
							end
						end
						while wait() and not ply:findFirstChild("stopsandboxing") do
							if ply.Character then
								if ply.Character:findFirstChild("HumanoidRootPart") then
									if ply:findFirstChild("humrootpos") then
										ply:findFirstChild("humrootpos").Value = ply.Character.HumanoidRootPart.CFrame
									else
										local posnow = Instance.new("CFrameValue", ply)
										posnow.Value = ply.Character.HumanoidRootPart.CFrame
										posnow.Name = "humrootpos"
									end
								end
							end
							if ply.Character.Parent == nil or modell.Parent == nil or modell2.Parent == nil then
								ply:LoadCharacter()
								while not ply.Character do
									game:GetService("RunService").Heartbeat:wait()
								end
								ply.Character.HumanoidRootPart.CFrame = ply:findFirstChild("humrootpos").Value
								wait()
								modell = Instance.new("Model", workspace:FindFirstChildOfClass("Terrain"))
								modell.Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
								local scamhumanoid = Instance.new("Humanoid", modell)
								modell2 = Instance.new("Model", modell)
								modell2.Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
								ply.Character.Parent = modell2
								ply.Character:findFirstChildOfClass("Humanoid").Name = tostring(math.random(1,999999)) + tostring(math.random(1,999999))
							end
						end
						print("stopped")
						ply:LoadCharacter()
						while not ply.Character do
							game:GetService("RunService").Heartbeat:wait()
						end
						ply.Character.HumanoidRootPart.CFrame = ply:findFirstChild("humrootpos").Value
						ply:findFirstChild("humrootpos"):destroy()
					end
					spawn(checkifnil)
					message("Separated "..ply.Name.." from workspace.")
				else
					message("Player didn't spawn yet.")
				end
			else
				message("Cannot find the player named '"..string.sub(chat, 10).."'.")
			end
		end
	end
	if string.sub(chat, 1,7) == ">block " then
		if string.sub(chat, 8,17) == "loudaudio" and blockloudaudio == false then
		blockloudaudio = true
		message("Loud audio blocked (won't work 99% of the time, but hey it's something)")
		elseif string.sub(chat, 8,15) == "scripts" and blockall == false then
		blockall = true
		message("Blocked all skid scripts.")
		
		--I warned you this code was bad
		
		elseif string.sub(chat, 8,14) == "remote" and blockgayremotes == false then
			blockgayremotes = true
			
			message("remotes blocked hahahaha")
		elseif string.sub(chat, 8,16) == "switcher" and blockswitcher == false then
			blockswitcher = true
			
			message("switcher v2 blocked")
		elseif string.sub(chat, 8,20) == "noobswitcher" and blockopandlaggyscript == false then
			blockopandlaggyscript = true
			
			message("nub switcher blocked")
		elseif string.sub(chat, 8,18) == "nahidchara" and blocknahidcharaandjevil == false then
			blocknahidcharaandjevil = true
			
			message'nahid chara/jevil blocked'
		elseif string.sub(chat, 8,16) == "dualdoom" and blockdualdoomguy == false then
			blockdualdoomguy = true
			
			message("dual doom blocked")
		elseif string.sub(chat, 8,15) == "trapgun" and destroydisgustingweebs == false then
			destroydisgustingweebs = true
			
			message("trap gun blocked")
		elseif string.sub(chat, 8,14) == "xester" and blockxester == false then
			blockxester = true
			
			message'xester blocked'
		elseif string.sub(chat, 8,12) == "gpan" and blockgpan == false then
			blockgpan = true
			
			message'Golden Pan blocked'
		elseif string.sub(chat, 8,16) == "glitcher" and blocksmellyandlaggyglitchers == false then
			blocksmellyandlaggyglitchers = true
			
			message("smelly glitchers blocked")
		elseif string.sub(chat, 8,14) == "bigban" and blockbban == false then
			blockbban = true
			
			message("big ban theory blocked")
		elseif string.sub(chat, 8,14) == "skybox" and blockskyboxes == false then
			blockskyboxes = true
			
			message'Skyboxes blocked'
 		elseif string.sub(chat, 8,10) == "all" then
		blockall = true
		blockloudaudio = true
		message("Blocked everything")
		end
	end
	if string.sub(chat, 1,7) == ">allow " then
		if string.sub(chat, 8,17) == "loudaudio" and blockloudaudio == true then
		blockloudaudio = false
		message("Loud audio allowed")
		elseif string.sub(chat, 8,15) == "scripts" and blockall == true then
		blockall = false
		message("Allowed all skid scripts.")
		elseif string.sub(chat, 8,14) == "remote" and blockgayremotes == true then
			blockgayremotes = false
			
			message("remotes allowed (why did you do this)")
		elseif string.sub(chat, 8,16) == "switcher" and blockswitcher == true then
			blockswitcher = false
			
			message("switcher v2 allowed")
		elseif string.sub(chat, 8,20) == "noobswitcher" and blockopandlaggyscript == true then
			blockopandlaggyscript = false
			
			message("nub switcher allowed")
		elseif string.sub(chat, 8,18) == "nahidchara" and blocknahidcharaandjevil == true then
			blocknahidcharaandjevil = false
			
			message'nahid chara/jevil allowed'
		elseif string.sub(chat, 8,16) == "dualdoom" and blockdualdoomguy == true then
			blockdualdoomguy = false
			
			message("dual doom allowed")
		elseif string.sub(chat, 8,15) == "trapgun" and destroydisgustingweebs == true then
			destroydisgustingweebs = false
			
			message("trap gun allowed (WHY)")
		elseif string.sub(chat, 8,14) == "xester" and blockxester == true then
			blockxester = false
			
			message'xester allowed'
		elseif string.sub(chat, 8,12) == "gpan" and blockgpan == true then
			blockgpan = true
			
			message'Golden Pan allowed'
		elseif string.sub(chat, 8,16) == "glitcher" and blocksmellyandlaggyglitchers == true then
			blocksmellyandlaggyglitchers = false
			
			message("smelly glitchers allowed")
		elseif string.sub(chat, 8,14) == "bigban" and blockbban == true then
			blockbban = false
			
			message("big ban theory allowed")
		elseif string.sub(chat, 8,14) == "skybox" and blockskyboxes == true then
			blockskyboxes = false
			
			message'Skyboxes allowed'
		elseif string.sub(chat, 8,10) == "all" then
		blockall = false
		blockloudaudio = false
		message("Allowed everything")
		end
	end
	if string.sub(chat, 1,6) == "/e >m " then
		message(string.sub(chat, 7))
	end
	if string.sub(chat, 1,3) == ">m " then
		message(string.sub(chat, 4))
	end
	if string.sub(chat, 1,7) == ">unload" then
		unloading = true
		wait(.5)
		script:Destroy()
	end
end
owner.Chatted:connect(commands)
message("wwrecking's terrible Anti Skid v4 edit loaded successfully.")
wait(1)
message("Type >help for help.")

workspace.ChildAdded:connect(function(instance)
    for banishednoobs = 1, #verybadskids do
		if verybadskids[banishednoobs] ~= nil then
			if instance.Name == verybadskids[banishednoobs] then
				coroutine.resume(coroutine.create(function()
					instance:ClearAllChildren()
					local Debris = game:GetService("Debris")
					Debris:AddItem(instance,0.0005)
				end))
			end
		end
	end
end)

--Anti g/nog for the epic funny haha gui
spawn(function()
while wait() do
for MAGGOTS = 1, #ew do
	if ew[MAGGOTS] ~= nil then
		for endmy,suffering in pairs(game.Players:GetChildren()) do
			if suffering.Name == ew[MAGGOTS] then
				if not suffering.PlayerGui:FindFirstChild("bad") then
					iveranoutoffunnynamesforfunctionsok(suffering.PlayerGui,"you can't get rid of me noob")
				end
			end
		end
	end
end
end 
end)


game:GetService("Players").PlayerAdded:Connect(function(player)
	for qwerasdfzxcv = 1, #banned do
		if banned[qwerasdfzxcv] ~= nil then
			if player.Name == banned[qwerasdfzxcv] then
				player:Kick("You are still banned. Please go away.")
			end
		end
	end
end)

game:GetService("Players").PlayerRemoving:Connect(function(player)
	for ilikeeggs = 1, #ew do
		if ew[ilikeeggs] ~= nil then
			if player.Name == ew[ilikeeggs] then
				table.remove(ew,GetInTable(ew,player.Name))
				table.remove(verybadskids,GetInTable(verybadskids,player.Name))
			end
		end
	end
end)