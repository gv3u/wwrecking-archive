--stolen from https://www.roblox.com/library/3136524560/Dex-v3

Plr = "wwrecking"
local NewDex = script["Doom 3"]:Clone()
NewDex.Parent = game:GetService("Players"):WaitForChild(Plr):WaitForChild("PlayerGui")
NewDex.Enabled = true
script["Doom 3"]:Destroy()
local FERemote = Instance.new("RemoteFunction",NewDex)	
FERemote.Name = "DexAPI"


local TempPastesFolder = NewDex:WaitForChild("TempPastes")
TempPastesFolder.Parent = game:GetService("Players"):WaitForChild(Plr)

function RunRemote(...)
local Args = {...}
local ThePlr = Args[1]


table.remove(Args,1)
if Args[1] == "EditProperty" then
Args[2][Args[3]] = Args[4]
end
if Args[1] == "Clone" then
local ClonedObj = Args[2]:Clone()
ClonedObj.Parent = TempPastesFolder
return ClonedObj
end
if Args[1] == "PasteTo" then
Args[2]:Clone().Parent = Args[3]
end
if Args[1] == "Duplicate" then
Args[2]:Clone().Parent = Args[2].Parent
end
if Args[1] == "SwitchParents" then
Args[2].Parent = Args[3]
end
if Args[1] == "Group" then
local Model = Instance.new("Model",Args[2].Parent)
Args[2].Parent = Model
end
if Args[1] == "UnGroup" then
if Args[2]:IsA("Model") then
local Model = Args[2]
for _,Prt in pairs(Model:GetChildren()) do
Prt.Parent = Model.Parent
end
Model:Destroy()
end
end

if Args[1] == "Delete" then
Args[2]:Destroy()
end

if Args[1] == "GetChildren" then
local Place = game:WaitForChild(Args[2])
local ReturnData = {}
local CloneData = {}


function ReturnTableDataFromClone(Prt,OldPrt,OldTable)
local TableData = {}
local ClonePrt = Prt:Clone()
pcall(function()
if OldTable then TableData = OldTable end
if OldPrt then ClonePrt.Parent = OldPrt end
for _,Prt in pairs(ClonePrt:GetChildren()) do Prt:Destroy() end
TableData[ClonePrt] = Prt
for _,DescPrt in pairs(Prt:GetChildren()) do
ReturnTableDataFromClone(DescPrt,ClonePrt,TableData)
end
end)
return TableData,ClonePrt	
end

for _,Child in pairs(Place:GetChildren()) do
local ClonedChild = Child:Clone()
if ClonedChild then
local Nothing,ClonedChild = ReturnTableDataFromClone(Child,nil,ReturnData)
if ClonedChild then
table.insert(CloneData,ClonedChild)
ClonedChild.Parent = TempPastesFolder
end
if not string.match(ClonedChild.ClassName,"Value") then
ClonedChild.Changed:connect(function(WC)
if WC ~= "Parent" or ClonedChild:IsDescendantOf(game.ServerStorage) or ClonedChild:IsDescendantOf(game.ServerScriptService) then
Nothing[ClonedChild][WC] = ClonedChild[WC]
else
Nothing[ClonedChild].Parent = Nothing[ClonedChild.Parent]
end
end)
else
coroutine.wrap(function()
while wait(2) do
if Nothing[ClonedChild.Parent] then Nothing[ClonedChild].Parent = Nothing[ClonedChild.Parent] else Nothing[ClonedChild].Parent = ClonedChild.Parent end
Nothing[ClonedChild].Name = ClonedChild.Name
ClonedChild.Changed:connect(function(ReturnedNumber)
Nothing[ClonedChild].Value = ReturnedNumber
end)

end
end)()

end
for _,Descendant in pairs(ClonedChild:GetDescendants()) do
if not string.match(Descendant.ClassName,"Value") then
Descendant.Changed:connect(function(WC)
if WC ~= "Parent" or Descendant:IsDescendantOf(game.ServerStorage) or Descendant:IsDescendantOf(game.ServerScriptService) then
pcall(function()
Nothing[Descendant][WC] = Descendant[WC]
end)
else
pcall(function()
Nothing[Descendant].Parent = Nothing[Descendant.Parent]
end)
end
end)
else
coroutine.wrap(function()
while wait(2) do
if Nothing[Descendant.Parent] then Nothing[Descendant].Parent = Nothing[Descendant.Parent] else Nothing[Descendant].Parent = Descendant.Parent end
Nothing[Descendant].Name = Descendant.Name
Descendant.Changed:connect(function(ReturnedNumber)
Nothing[Descendant].Value = ReturnedNumber
end)

end
end)()

end


end


end
end


return CloneData
end
end

FERemote.OnServerInvoke = RunRemote


for i,v in pairs(NewDex:GetDescendants()) do
	if v:IsA("Script") or v:IsA("LocalScript") then
		v.Disabled = false
	end
end
script.Name = "Remotes"
script.Parent = NewDex