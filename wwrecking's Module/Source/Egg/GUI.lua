--GUI Stuff


local function IHopeYouKnowWhatYoureDoing()
	script.Parent.Warning:Destroy()
	script.Parent.Frame.Visible = true
	script.Parent.Frame.Draggable = true
	local box = script.Parent.Frame.TextBox
	box.Text = ""
	
	box.FocusLost:connect(function(ep)
		if box.Text ~= "" and ep then
			script.Parent.Remote.ExecuteSkidScript:FireServer(box.text)
			box.Text = ""
		end
	end)
end


if game.PlaceId == 843495510 then
	script.Parent.Warning.Visible = true
	local msg = script.Parent.Warning.msg
	msg.Text = [[
Warning!
This is a BANNED script/module!
I hope to god you know what you're doing.
Are you SURE you want to run this unholy monstrosity?
]]
	script.Parent.Warning.Decision.Yes.MouseButton1Click:connect(function()
		IHopeYouKnowWhatYoureDoing()
	end)
	script.Parent.Warning.Decision.No.MouseButton1Click:connect(function()
		script.Parent:Destroy()
	end)
else
	IHopeYouKnowWhatYoureDoing()
end