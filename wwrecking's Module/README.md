# wwrecking's Module
This is the complete, unobfuscated source code to wwrecking's Module, including the Notive and Anti Skid v4 edits.  
This module contains very unorganized and **HORRIBLE** code. Keep in mind, I read no tutorials whatsoever when I made these scripts, so it's obvious that these scripts will be cringe.  
This is here only for archival purposes.

## Description:
This is basically just a beefed up version of that module everyone uses to load their scripts. (the one with the script/ command)  
Cleaned up for release since I have no use for it anymore.  
Oh yeah, you might want to replace some references to my name (e.g. game.Players.wwrecking) with yours, since I intended this to be a personal module.
This is no longer in development due to it being banned from Void SB.

## HOW TO USE:
Download the rbxm file [here](https://gitlab.com/gv3u/wwrecking-archive/-/blob/master/wwrecking's%20Module/Release/wwrecking's%20Module.rbxm) (the git only contains the .lua files for easy browsing, and the version uploaded to Roblox is obfuscated), and upload it to Roblox via Studio.  
Load it by typing `require(ID)("your name")` into a script builder or server side game or whatever.  
Add your scripts in the SkidScripts folder.  
Add scripts that you want in ServerScriptService into the globalScripts value.  
Type the script name into the "wwrecking's Module" box.  
Have fun.
