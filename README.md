# wwrecking Archive

This serves as a museum for everything done under the wwrecking account, including scripts I made for Void SB.  
I'm actually very embarassed to release this to the public, but I feel like I have to for transparency and preservation purposes.

I strongly suggest reading the [History of wwrecking](https://gitlab.com/gv3u/wwrecking-archive/-/blob/master/History%20of%20wwrecking.md), which is a brief summary of what the wwrecking account was and what I did with it.  

Keep in mind I'm still clueless when it comes to basically anything, so this whole repository is terrible and can be improved. Honestly, I don't even like GitLab that much. I only really liked it for the Web IDE. I started this repository on GitLab sometime in 2020, and I'm too lazy to migrate this to GitHub.  
This whole thing was prepared in advance, and I've been meaning to do a big update, but I was too nervous to log into wwrecking again.
If you can improve formatting or whatever, feel free to send a pull request.  

I *know* GitLab isn't the best place to host this, and I'm not very familar with GitLab or GitHub.

If you want to draw fanart of wwrecking for whatever reason, please use the current avatar, which was inspired by Builderman's old avatar. I'm keeping that avatar as I feel it fits with the 2007 aesthetic, and the crying face perfectly how I feel about wwrecking, and my past in general.
